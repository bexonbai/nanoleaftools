//
//  PairingModeViewModel.swift
//  NanoleafTools
//
//  Created by Bexon Pak on 2022/1/28.
//

import Foundation
import CoreNFC

class NFCInfoViewModel: ObservableObject {
  @Published var isNfcSupport = false
  @Published var isError = false
  @Published var errorMessage = ""
  @Published var nanoTag: NanoleafNFCTag?
  
  var helper: NFCHelper?
  
  init() {
    checkNfc()
  }
  
  // ----------------------------------
  //  MARK: - Setup -
  //
  func checkNfc() {
    if #available(iOS 11.0, *) {
      if NFCNDEFReaderSession.readingAvailable {
        isNfcSupport = true
        didTapReadNFC()
      } else {
        isNfcSupport = false
      }
    } else {
      isNfcSupport = false
    }
  }
  
  // 当 NFCHelper 已经处理过或者通信失败时调用
    func onNFCResult(success: Bool, messages: Any) {
      if success {
        DispatchQueue.main.async {
          print(NanoleafNFCTag(messages as! NFCNDEFMessage))
          self.nanoTag = NanoleafNFCTag(messages as! NFCNDEFMessage)
        }
      } else {
        print("error", messages)
        DispatchQueue.main.async {
          self.isError = true
          self.errorMessage = "\(messages)"
        }
      }
    }
  
  func didTapReadNFC() {
      if helper == nil {
        helper = NFCHelper()
        helper?.onNFCResult = self.onNFCResult(success:messages:)
      }
      helper?.restartSession()
    }
}
