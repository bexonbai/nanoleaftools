//
//  URL+Extension.swift
//  NanoleafTools
//
//  Created by Bexon Pak on 2022/2/8.
//

import Foundation

extension URL {
    func valueOf(_ queryParamaterName: String) -> String? {
        guard let url = URLComponents(string: self.absoluteString) else { return nil }
        return url.queryItems?.first(where: { $0.name == queryParamaterName })?.value
    }
}
