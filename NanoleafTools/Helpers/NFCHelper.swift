//
//  NFCHelper.swift
//  NanoleafTools
//
//  Created by Bexon Pak on 2022/2/8.
//

import Foundation
import CoreNFC

class NFCHelper: NSObject, NFCNDEFReaderSessionDelegate {
  
  var onNFCResult: ((Bool, Any) -> ())?
  
  func restartSession() {
    let session = NFCNDEFReaderSession(delegate: self, queue: nil, invalidateAfterFirstRead: true)
    session.alertMessage = NSLocalizedString("Tap your iPhone to the tap area on Control Square", comment: "")
    session.begin()
  }
  
  // MARK: NFCNDEFReaderSessionDelegate
  func readerSession(_ session: NFCNDEFReaderSession, didInvalidateWithError error: Error) {
    guard let onNFCResult = onNFCResult else {
      return
    }
    if let readerError = error as? NFCReaderError {
      if (readerError.code != .readerSessionInvalidationErrorFirstNDEFTagRead)
          && (readerError.code != .readerSessionInvalidationErrorUserCanceled) {
        onNFCResult(false, error)
        session.alertMessage = error.localizedDescription
      }
    }
  }
  
  func readerSession(_ session: NFCNDEFReaderSession, didDetectNDEFs messages: [NFCNDEFMessage]) {
    guard let onNFCResult = onNFCResult else {
      return
    }
    if(!messages.isEmpty && messages.first != nil) {
      onNFCResult(true, messages.first!)
      session.alertMessage = NSLocalizedString("Success", comment: "")
    }
  }
}
