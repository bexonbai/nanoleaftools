//
//  NanoleafToolsApp.swift
//  NanoleafTools
//
//  Created by Bexon Pak on 2022/1/28.
//

import SwiftUI

@main
struct NanoleafToolsApp: App {
  var body: some Scene {
    WindowGroup {
      HomeView()
            .accentColor(Color.init(red: 68 / 255, green: 172 / 255, blue: 51 / 255))
    }
  }
}
