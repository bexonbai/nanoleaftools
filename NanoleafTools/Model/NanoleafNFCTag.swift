//
//  NanoleafNFCTag.swift
//  NanoleafTools
//
//  Created by Bexon Pak on 2022/2/8.
//

import Foundation
import CoreNFC

struct NanoleafNFCTag: Identifiable {
  var id = UUID()
  var payload: [NFCNDEFPayload]
  var isNanoleafDevice: Bool = false
  var xhm: URL? // HomeKit Pairing Link
  var url: URL? // Nanoleaf App URL Scheme
  var pairingCode: String?
  var serialNumber: String?
  var defaultName: String?
  
  init(_ message: NFCNDEFMessage) {
    self.payload = message.records.reversed()
    var xhmString: String?
    var nanoleafSetupURL: String?
    
    for records in self.payload {
      guard let urlPayload = records.wellKnownTypeURIPayload() else {
        return
      }
      let dataString = urlPayload.absoluteString
      if (dataString.starts(with: "X-HM")) {
        xhmString = dataString
      } else if dataString.starts(with: "https://nanoleaf.me/app/setup") {
        nanoleafSetupURL = dataString
      }
    }
    if (xhmString != nil && nanoleafSetupURL != nil) {
      self.isNanoleafDevice = true
      self.xhm = URL(string: xhmString!)
      self.url = URL(string: nanoleafSetupURL!)
      self.serialNumber = self.url?.valueOf("sn")
      self.pairingCode = self.url?.valueOf("pc")
      let name = self.url?.valueOf("mc")?.suffix(4)
      if name != nil {
        self.defaultName = String(name!)
      }
    }
  }
}
