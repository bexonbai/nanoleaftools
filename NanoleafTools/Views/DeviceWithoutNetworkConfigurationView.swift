//
//  DeviceWithoutNetworkConfigurationView.swift
//  NanoleafTools
//
//  Created by Bexon Pak on 2022/2/8.
//

import SwiftUI

struct DeviceWithoutNetworkConfigurationView: View {
    var body: some View {
        Text(LocalizedStringKey("Device Without Network Configuration"))
    }
}

struct DeviceWithoutNetworkConfigurationView_Previews: PreviewProvider {
    static var previews: some View {
        DeviceWithoutNetworkConfigurationView()
    }
}
