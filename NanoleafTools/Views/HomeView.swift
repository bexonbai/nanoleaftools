//
//  ContentView.swift
//  NanoleafTools
//
//  Created by Bexon Pak on 2022/1/28.
//

import SwiftUI

struct HomeView: View {
  var body: some View {
    NavigationView {
      List {
        NavigationLink(destination: NFCInfoView()) {
          Text(LocalizedStringKey("NFC Information"))
        }
          NavigationLink(destination: DeviceWithoutNetworkConfigurationView()) {
            Text(LocalizedStringKey("Device Without Network Configuration"))
          }
      }
      .navigationTitle("Nanoleaf Tools")
    }
  }
}

struct HomeView_Previews: PreviewProvider {
  static var previews: some View {
    HomeView()
  }
}
