//
//  ParingModeView.swift
//  NanoleafTools
//
//  Created by Bexon Pak on 2022/1/28.
//

import SwiftUI
import CoreNFC

struct NFCInfoView: View {
  @StateObject var viewModel = NFCInfoViewModel()
  var body: some View {
    HStack{
      VStack {
        if (viewModel.isNfcSupport) {
          NFCInfoTapView(viewModel: viewModel)
        } else {
          NFCUnsupportedView()
        }
        Spacer()
      }
    }
  }
}

struct NFCInfoTapView: View {
  @StateObject var viewModel: NFCInfoViewModel
  var body: some View {
    HStack {
      VStack {
        if(viewModel.nanoTag != nil) {
          if (viewModel.nanoTag!.isNanoleafDevice) {
            NanoleafNFCRecordItemView(nanoTag: viewModel.nanoTag!)
          } else {
            NFCRecordItemView(nanoTag: viewModel.nanoTag!)
          }
        } else {
          List { }
        }
        Spacer()
        Button {
          viewModel.didTapReadNFC()
        } label: {
          Spacer()
          Text("Scan & Show")
            .fontWeight(.bold)
            .padding(.vertical, 13)
          Spacer()
        }
        .buttonStyle(.borderedProminent)
        .padding()
      }
    }
    .alert("Error", isPresented: $viewModel.isError) {
      Button {
        viewModel.isError = false
      } label: {
        Text("Demiss")
      }
    } message: {
      Text(viewModel.errorMessage)
    }
  }
}

struct NFCUnsupportedView: View {
  var body: some View {
    VStack(alignment: .center, spacing: 0) {
      Spacer()
      HStack {
        Spacer()
        Text("Device NFC is NOT available").foregroundColor(.red).fontWeight(.bold)
        Spacer()
      }
      Spacer()
    }
  }
}

struct NanoleafNFCRecordItemView: View {
  var nanoTag: NanoleafNFCTag
  var body: some View {
    VStack {
      Form {
        if (nanoTag.xhm != nil) {
          Section(LocalizedStringKey("HomeKit Pairing Link")) {
            Text(nanoTag.xhm!.absoluteString)
              .contextMenu(ContextMenu(menuItems: {
                Button("Copy", action: {
                  UIPasteboard.general.string = nanoTag.xhm!.absoluteString
                })
              }))
          }
        }
        if (nanoTag.url != nil) {
          Section(LocalizedStringKey("Nanoleaf App Pairing Link")) {
            Text(nanoTag.url!.absoluteString)
              .contextMenu(ContextMenu(menuItems: {
                Button("Copy", action: {
                  UIPasteboard.general.string = nanoTag.url!.absoluteString
                })
              }))
          }
        }
        if (nanoTag.defaultName != nil) {
          Section(LocalizedStringKey("Default Name")) {
            Text(nanoTag.defaultName!)
              .contextMenu(ContextMenu(menuItems: {
                Button("Copy", action: {
                  UIPasteboard.general.string = nanoTag.defaultName!
                })
              }))
          }
        }
        if (nanoTag.pairingCode != nil) {
          Section(LocalizedStringKey("Pairing Code")) {
            Text(nanoTag.pairingCode!)
              .contextMenu(ContextMenu(menuItems: {
                Button("Copy", action: {
                  UIPasteboard.general.string = nanoTag.pairingCode!
                })
              }))
          }
        }
        if (nanoTag.serialNumber != nil) {
          Section(LocalizedStringKey("Serial Number")) {
            Text(nanoTag.serialNumber!)
              .contextMenu(ContextMenu(menuItems: {
                Button("Copy", action: {
                  UIPasteboard.general.string = nanoTag.serialNumber!
                })
              }))
          }
        }
        
        Section(LocalizedStringKey("Raw Records")) {
          ForEach(nanoTag.payload, id: \.self) { records in
            Text("\(String.init(data: records.payload, encoding: .utf8) ?? "")")
              .contextMenu(ContextMenu(menuItems: {
                Button("Copy", action: {
                  UIPasteboard.general.string = String.init(data: records.payload, encoding: .utf8) ?? ""
                })
              }))
          }
        }
      }.navigationTitle(LocalizedStringKey("Nanoleaf NFC Records"))
    }
  }
}

struct NFCRecordItemView: View {
  @State var nanoTag: NanoleafNFCTag
  var body: some View {
    VStack {
      List {
        ForEach(nanoTag.payload, id: \.self) { records in
          Text("\(String.init(data: records.payload, encoding: .utf8) ?? "")")
            .contextMenu(ContextMenu(menuItems: {
              Button("Copy", action: {
                UIPasteboard.general.string = "\(String.init(data: records.payload, encoding: .utf8) ?? "")"
              })
            }))
        }
      }
      .navigationTitle(LocalizedStringKey("Records"))
    }
  }
}

struct PairingModeView_Previews: PreviewProvider {
  @StateObject var viewModel = NFCInfoViewModel()
  static var previews: some View {
    NFCUnsupportedView()
  }
}
